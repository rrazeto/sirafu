package cl.sirafu.sirafucore.service;

import cl.sirafu.sirafujpa.entity.UsuarioSistema;

public interface LoginService {

	public UsuarioSistema findByUserId(String userId);
}
