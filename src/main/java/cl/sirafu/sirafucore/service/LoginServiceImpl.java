package cl.sirafu.sirafucore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.sirafu.sirafujpa.entity.UsuarioSistema;
import cl.sirafu.sirafujpa.repository.FuncionarioSistemaRepository;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    @Autowired
    private FuncionarioSistemaRepository funcionarioSistemaRepository;

    @Override
    public UsuarioSistema findByUserId(String userId) {

    	UsuarioSistema usuarioSistema;

        try {
        	
        	usuarioSistema = funcionarioSistemaRepository.findOne(Long.valueOf(userId));            

        }catch(Exception ex){
        	
        	usuarioSistema = null;
        }

        return usuarioSistema;
    }
}
