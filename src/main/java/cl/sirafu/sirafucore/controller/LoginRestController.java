package cl.sirafu.sirafucore.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.sirafu.sirafucore.service.LoginService;
import cl.sirafu.sirafujpa.entity.UsuarioSistema;

@RestController
@RequestMapping(value = "login")
public class LoginRestController {

	private static final Logger logger = Logger.getLogger(LoginRestController.class);

	@Autowired
	private LoginService loginService;

	// @Autowired
	// private CustomController controller;

	@RequestMapping(value = "/usuario/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getPerfilUsuarioTest(@PathVariable("userId") String userId) {         

    	UsuarioSistema usuarioSistema = loginService.findByUserId(userId);









    	
    	if(usuarioSistema == null){
    		
    		return "No existe usuario";
    	}
    	
        return loginService.findByUserId(userId);
    }
}
