package cl.sirafu.sirafujpa.repository;

import cl.sirafu.sirafujpa.base.BaseRepository;
import cl.sirafu.sirafujpa.entity.Perfil;

public interface PerfilRepository extends BaseRepository<Perfil, Long> {
}
