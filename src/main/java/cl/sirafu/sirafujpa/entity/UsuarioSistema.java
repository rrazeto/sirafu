package cl.sirafu.sirafujpa.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.*;

@Entity
@Table(name = "USUARIO_SISTEMA")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsuarioSistema implements java.io.Serializable {

    private static final long serialVersionUID = 4212280655032713305L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ussiId;

    private String ussiUserid;

    private String ussiCorreoElectronico;

    private String ussiRut;

    private String ussiNombre;

    private String ussiAPaterno;

    private String ussiAMaterno;

    private Boolean ussiActivo = Boolean.TRUE;

    @OneToOne
    @JoinColumn(name = "PERF_ID")
    private Perfil perfil;

	public Long getUssiId() {
		return ussiId;
	}

	public void setUssiId(Long ussiId) {
		this.ussiId = ussiId;
	}

	public String getUssiUserid() {
		return ussiUserid;
	}

	public void setUssiUserid(String ussiUserid) {
		this.ussiUserid = ussiUserid;
	}

	public String getUssiCorreoElectronico() {
		return ussiCorreoElectronico;
	}

	public void setUssiCorreoElectronico(String ussiCorreoElectronico) {
		this.ussiCorreoElectronico = ussiCorreoElectronico;
	}

	public String getUssiRut() {
		return ussiRut;
	}

	public void setUssiRut(String ussiRut) {
		this.ussiRut = ussiRut;
	}

	public String getUssiNombre() {
		return ussiNombre;
	}

	public void setUssiNombre(String ussiNombre) {
		this.ussiNombre = ussiNombre;
	}

	public String getUssiAPaterno() {
		return ussiAPaterno;
	}

	public void setUssiAPaterno(String ussiAPaterno) {
		this.ussiAPaterno = ussiAPaterno;
	}

	public String getUssiAMaterno() {
		return ussiAMaterno;
	}

	public void setUssiAMaterno(String ussiAMaterno) {
		this.ussiAMaterno = ussiAMaterno;
	}

	public Boolean getUssiActivo() {
		return ussiActivo;
	}

	public void setUssiActivo(Boolean ussiActivo) {
		this.ussiActivo = ussiActivo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
}