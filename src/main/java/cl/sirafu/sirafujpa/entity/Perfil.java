package cl.sirafu.sirafujpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "PERFIL")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Perfil implements java.io.Serializable {

	private static final long serialVersionUID = -4930282786870837374L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long perfId;

	private String perfDescripcion;

	public Long getPerfId() {
		return perfId;
	}

	public void setPerfId(Long perfId) {
		this.perfId = perfId;
	}

	public String getPerfDescripcion() {
		return perfDescripcion;
	}

	public void setPerfDescripcion(String perfDescripcion) {
		this.perfDescripcion = perfDescripcion;
	}
}